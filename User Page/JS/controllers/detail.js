const getId = (id) => document.getElementById(id);
const getClass = (id) => document.getElementsByClassName(id)[1];
// ON LOAD
// window.onload = function () {
//   getId("product__content").style.transform = "translate(-50%, -20%)";
//   //   getEle("myTask").style.opacity = "1";
// };
let prDetail = [];
const fetchProductDetail = () => {
  let id = location.search.split("=")[1];
  const promise = axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/" + id,
    method: "GET",
  });
  promise
    .then((res) => {
      prDetail = res.data;
      console.log(prDetail);
      renderDetail(prDetail);
    })
    .catch((err) => {
      console.log(err);
    });
};
const renderDetail = (listDetail) => {
  let contentDetail = "";
  contentDetail += `
	  <div class="row">
             <div class="col-md-6">
               <div class="detail__product__img" id="product__image">
                 <img
                   src="${listDetail.image}"
                   alt=""
                   class="d-block w-100"
                 />
               </div>
             </div>
             <div class="col-md-6">
               <div class="detail__product__content" id="product__content">
                 <h5 class="display-4 text-center">${listDetail.name}</h5>
                 <ul>
                   <li>Description: ${listDetail.description}</li>
                   <li>Price: ${listDetail.price}</li>
                   <li>Inventory: ${listDetail.inventory}</li>
                   <li>Rating: ${listDetail.rating}</li>
                   <li>Type: ${listDetail.type}</li>
                 </ul>
               </div>
             </div>
           </div>
	  `;
  getId("productDetail").innerHTML = contentDetail;
};
fetchProductDetail();
