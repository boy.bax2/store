// GET ID
const getId = (id) => document.getElementById(id);
// GET CLASS
const getClass = (id) => document.getElementsByClassName(id)[0];
// Tạo biến cho API constructor
const productService = new getAPI();
// Tạo mảng chứa list product
let prList = [];
// FUNCTION CHECK ID
findId = (id) => {
  for (let i = 0; i < prList.length; i++) {
    if (prList[i].id == id) {
      return i;
    }
  }
  return -1;
};
// Function get list product
const getListPr = () => {
  productService
    .getList()
    .then((res) => {
      prList = res.data;
      //   console.log(prList);
      renderPr(prList);
      findProduct();
      //   renderPr(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
getListPr();
// FUNCTION VIEW DETAIL PRODUCT
const viewDetail = (id) => {
  console.log(id);
  window.location.assign("detail.html?id=" + id);
};
// FUNCTION RENDER PRODUCT
const renderPr = (listPr) => {
  let contentList = "";
  for (let product of listPr) {
    // console.log(product.id);
    contentList += `
	<div class="item text-center product__item">
            <div class="card">
              <img
                class="card-img-top"
                src="${product.image}"
                alt="item"
				style="display: block; width: 95%; height: 250px; margin: 0 auto;"
              />
              <div class="card-body">
                <h5 class="card-title">${product.name}</h5>
                <p class="card-text">
                  ${"Price: "}${product.price}${" VND"}
                </p>
			  </div>
			</div>
			<div class="product__item__overlay"></div>
			<div class="product__item__icon"> 
			<button class="product__item__more btn btn-primary" onclick="viewDetail(${
        product.id
      })">
      			<i class="fa fa-eye"></i>
      				SEE MORE
			</button>
			<button class="product__item__buy btn btn-primary" onclick ="getProduct('${
        product.id
      }')">
			<i class="fa fa-cart-plus"></i>
      				ADD TO CART
			</button>
			</div>
          </div>
	 `;
  }
  getId("productList").innerHTML = contentList;
  // OWL CAROUSEL
  $(".owl-carousel").owlCarousel({
    loop: true,
    margin: 10,
    mouseDrag: true,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayTimeout: 2000,
    smartSpeed: 500,
    nav: true,
    // dots: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 4,
      },
    },
  });
};

// FUNCTION FIND PRODUCT
const findProduct = () => {
  let result = [];
  let keyWord = getId("txtSearch").value;
  for (let product in prList) {
    let nameProduct = prList[product].name;
    nameProduct = nonAccentVietnamese(nameProduct);
    // console.log(nameProduct);
    keyWord = nonAccentVietnamese(keyWord).trim();
    if (nameProduct.indexOf(keyWord) !== -1) {
      result.push(prList[product]);
    }
  }

  renderPrDetail(result);
  getId("searchContent").style.display = keyWord ? "block" : "none";
};

// FUNCTION VIETNAMESE
function nonAccentVietnamese(str) {
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
  str = str.replace(/đ/g, "d");
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // Huyền sắc hỏi ngã nặng
  str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // Â, Ê, Ă, Ơ, Ư
  //   console.log(str);
  return str;
}
// FUNCTION RENDER PRODUCT DETAIL
const renderPrDetail = (result) => {
  detailContent = "";
  for (product of result) {
    detailContent += `
	<div
	class="search__content__item"
  >
	<div class="row">
	  <div class="col-md-4">
	  <div class="search__content__item__img">
	  <img
	  src="${product.image}"
	  alt=""
	/>
		</div>
		
	  </div>
	  <div class="col-md-8">
		<div class="search__content__item__name">
		  <h5>${product.name}</h5>
		</div>
	  </div>
	</div>
	<div class="search__content__item__overlay"></div>
	<button class="product__item__more btn btn-primary" onclick="viewDetail('${product.id}')">
      			<i class="fa fa-eye"></i>
      				SEE MORE
			</button>
  </div>
	`;
  }
  getId("searchContent").innerHTML = detailContent;
};
// Tạo mảng rỗng chứa product bỏ zô Cart
let productCart = [];

// FUNCTION GET PRODUCT, THEN ADD IN CART
const getProduct = (id) => {
  //   const index = findId(id);
  //   if (index !== -1) {
  //     // console.log(prList[index]);
  //     const newProduct = new ProductCart(
  //       new Product(
  //         prList[index].id,
  //         prList[index].name,
  //         prList[index].image,
  //         prList[index].description,
  //         prList[index].price,
  //         prList[index].inventory,
  //         prList[index].rating,
  //         prList[index].type
  //       ),
  //       1
  //     );
  //     productCart.push(newProduct);
  //   }
  if (checkCartItem(id) === -1) {
    let newProduct = new ProductCart(prList[findId(id)], 1);
    // productCart.push(newProduct);
    console.log("Ok");
  } else {
    console.log("Sp nay da them");
    productCart[checkCartItem(id)].quantity += 1;
  }
  renderPrCart(productCart);
  saveData();
  // console.log(productCart);
};
// FUNCTION CHECK CART ITEM
const checkCartItem = (id) => {
  productCart.forEach((item, index) => {
    if (id === item.phone.id) {
      return index;
    }
  });
  return -1;
};
// CHANGE CONTENT MODAL CART
getId("productCart").addEventListener("click", () => {
  document.getElementsByClassName("modalCartTitle")[0].innerHTML =
    "Shopping Phone";
  document.getElementsByClassName(
    "modalCartFooter"
  )[0].innerHTML = `<button class="btn btn-info" onclick= "addUser()">Total</button>`;
});
// FUNCTION RENDER PRODUCT CART
// console.log(productCart)
// console.log(productCart);
const renderPrCart = () => {
  let contentCart = "";
  for (let product of productCart) {
    // console.log(product.phone.id);
    contentCart += `
   		<div class="product__cart__item border-bottom">
   						<div class="row">
   						  <div class="col-md-1">
   							<div class="product__cart__item__cancel  cart__item">
   							  <button class="btn btn-danger" onclick="deletePr(${product.phone.id})">
   								<i class="fa fa-trash-alt"></i>
   							  </button>
   							</div>
   						  </div>
   						  <div class="col-md-3">
   							<div class="product__cart__item__img cart__item">
   							  <img
   								src="${product.phone.image}"
   								style="width: 60px"
   								alt=""
   							  />
   							</div>
   						  </div>
   						  <div class="col-md-3">
   							<div class="product__cart__item__img cart__item">
   							  ${product.phone.name}
   							</div>
   						  </div>
   						  <div class="col-md-3">
   							<div class="product__cart__item__change cart__item">
   							  <button class="btn btn-primary change__plus" onclick="plusPr(${
                    product.phone.id
                  })"><i class="fa fa-plus"></i></button>
  							  <span class="mx-4" id="plusProduct">${product.quantity}</span>
  							  <button class="btn btn-primary change__sub"><i class="fa fa-minus"></i></button>
  							</div>
  						  </div>
  						  <div class="col-md-2">
  							<div class="product__cart__item__price cart__item">$${
                  product.phone.price * product.quantity
                }</div>
  						  </div>
  						</div>
  					  </div>
  		`;
  }

  getId("prCart").innerHTML = contentCart;
  //   console.log(contentCart);
};
// FUNCTION SAVE DATA PRODUCT CART IN STORAGE
const saveData = () => {
  localStorage.setItem("productCart", JSON.stringify(productCart));
};
// FUNCTION GET DATA
const fetchData = () => {
  const localProduct = localStorage.getItem("productCart");
  if (localProduct) {
    productCart = JSON.parse(localProduct);
  } else {
    return;
  }
  renderPrCart(productCart);
};
// FUNCTION DELETE PRODUCT CART
const deletePr = (id) => {
  //   console.log(id);
  //   console.log(findCartById(id));
  productCart.splice(findCartById(id), 1);
  renderPrCart(productCart);
  saveData();
};
// find cart by id
const findCartById = (id) => {
  productCart.forEach((item, index) => {
    if (item.phone.id == id) {
      console.log(index);
      return index;
    }
  });
  return -1;
};
fetchData();
